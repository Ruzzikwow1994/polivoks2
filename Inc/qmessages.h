#ifndef __QMSG_H__
#define __QMSG_H__
#include "stm32f0xx_hal.h"
typedef struct 
{
  uint8_t cmd;
  uint8_t channel;
  uint8_t data[2];
} MIDI_MESSAGE;

void  QCreateQueueMsgs(uint8_t size_of_queue);
uint8_t QGetMsg(MIDI_MESSAGE* msgMIDI);
void QAddMsg(MIDI_MESSAGE msgMIDI);

#endif /* __QMSG_H__ */
