/** Author: sdaware@mac.com
  ******************************************************************************
  * File Name          : ParamDataFlash.h
  * Date               : 24/06/2016 
  * Description        : ������� ������ � ���� �������. 
  *                      
  ******************************************************************************
  *            ���������:
  *
  *             void Storage_InitParams(void)
  *             void Storage_SetParams(void)    -������ ���������� �� ����
  *             
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __paramdataflash_H
#define __paramdataflash_H
#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f0xx_hal.h"

// Flash Data Storage ----------------------------------------------------------
extern uint32_t extMem[];

// ���������(�������� �� ����) �������� ����� ������ �����
// ��� �������� ����� ���������� � ������� ��������� � ���.
typedef struct {
// ����� ������ ����� ������
uint32_t        MemBlockSign;  
// ������� �����
int8_t         Channel_curr;

} PARAMS_DEVICE;

// ���������(�������) �������� ����� ������ �����
extern PARAMS_DEVICE params;

// ���������� ��������� ������ - ��������� ���������� - ��� �������� �� ����
// ���� ������ ���� �� ����, �� ������������ ������������� ���������� �������
// � ���� ������
void Storage_InitParams(void);
// ������ ���������� �� ����, ���������� ����� ������ InitParamsStorage
void Storage_SetParams(void);
// -----------------------------------------------------------------------------

#ifdef __cplusplus
}
#endif
#endif /* __paramdataflash_H */

/************************ (C) COPYRIGHT ��������� **************END OF FILE****/
