#ifndef __QNOTES_H__
#define __QNOTES_H__

#include "stm32f0xx_hal.h"

void    QNoteCreateQueue(uint8_t size_of_queue);
void    QNoteDel(uint8_t note);
void    QNoteAdd(uint8_t note);
uint8_t QNoteGetTop();
uint8_t QNoteGetPreTop();

#endif /* __QNOTES_H__ */
