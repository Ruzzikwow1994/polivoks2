

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __USBD_MIDI_IF_H
#define __USBD_MIDI_IF_H

/* Includes ------------------------------------------------------------------*/
#include "usbd_midi.h"

extern USBD_MIDI_ItfTypeDef  USBD_Interface_fops_HS;


uint8_t MIDI_Transmit_HS(uint8_t* Buf, uint16_t Len);



#endif /* __USBD_MIDI_IF_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
