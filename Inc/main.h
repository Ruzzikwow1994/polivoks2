/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f0xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define KEY_LEARN_Pin GPIO_PIN_13
#define KEY_LEARN_GPIO_Port GPIOC
#define OUT_GATE1_Pin GPIO_PIN_0
#define OUT_GATE1_GPIO_Port GPIOB
#define OUT_GATE2_Pin GPIO_PIN_1
#define OUT_GATE2_GPIO_Port GPIOB
#define LED_STATE_Pin GPIO_PIN_2
#define LED_STATE_GPIO_Port GPIOB
#define LED_NOTE_ON_Pin GPIO_PIN_10
#define LED_NOTE_ON_GPIO_Port GPIOB
#define LDAC_Pin GPIO_PIN_11
#define LDAC_GPIO_Port GPIOB
#define SYNC_Pin GPIO_PIN_3
#define SYNC_GPIO_Port GPIOB
#define GATE_Pin GPIO_PIN_4
#define GATE_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */
#define MIDI_CIN_NOTE_OFF		(uint8_t)(0x00)//++
#define MIDI_CIN_NOTE_ON		(uint8_t)(0x01)//++
#define MIDI_CIN_CONTROL_CHANGE		(uint8_t)(0x03)//++
#define MIDI_CIN_PITCHBEND_CHANGE	(uint8_t)(0x06)//++

#define MIDI_CIN_POLY_KEYPRESS		(uint8_t)(0x0A)//++
#define MIDI_CIN_PROGRAM_CHANGE		(uint8_t)(0x0C)//++
#define MIDI_CIN_CHANNEL_PRESSURE	(uint8_t)(0x0D)

#define DAC_OUT HAL_GPIO_WritePin(LDAC_GPIO_Port,LDAC_Pin,GPIO_PIN_RESET);HAL_GPIO_WritePin(LDAC_GPIO_Port,LDAC_Pin,GPIO_PIN_SET)
#define CS_LOW HAL_GPIO_WritePin(GPIOB,GPIO_PIN_12, GPIO_PIN_RESET)
#define CS_HIGH HAL_GPIO_WritePin(GPIOB,GPIO_PIN_12, GPIO_PIN_SET)
/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
