
#include "usbd_midi_if.h"
#include "usbd_midi.h"
#include "qmessages.h"

#define USB_BUF_SIZE 16
uint8_t usb_rx[USB_BUF_SIZE];


/* Define size for the receive and transmit buffer over MIDI */
/* It's up to user to redefine and/or remove those define */
#define APP_RX_DATA_SIZE  64
#define APP_TX_DATA_SIZE  64

/* Create buffer for reception and transmission           */
/* It's up to user to redefine and/or remove those define */
/** Received data over USB are stored in this buffer      */
uint8_t UserRxBufferHS[APP_RX_DATA_SIZE];

/** Data to send over USB MIDI are stored in this buffer   */
uint8_t UserTxBufferHS[APP_TX_DATA_SIZE];


/**
	* @}
	*/

/** @defgroup USBD_MIDI_IF_Exported_Variables USBD_MIDI_IF_Exported_Variables
	* @brief Public variables.
	* @{
	*/

extern USBD_HandleTypeDef hUsbDeviceFS;

/**
	* @}
	*/

/** @defgroup USBD_MIDI_IF_Private_FunctionPrototypes USBD_MIDI_IF_Private_FunctionPrototypes
	* @brief Private functions declaration.
	* @{
	*/

static int8_t MIDI_Init_HS(void);
static int8_t MIDI_DeInit_HS(void);
static int8_t MIDI_Control_HS(uint8_t cmd, uint8_t* pbuf, uint16_t length);
static int8_t MIDI_Receive_HS(uint8_t* pbuf, uint32_t *Len);



/**
	* @}
	*/

USBD_MIDI_ItfTypeDef USBD_Interface_fops_HS =
{
	MIDI_Init_HS,
	MIDI_DeInit_HS,
	MIDI_Control_HS,
	MIDI_Receive_HS
};

/* Private functions ---------------------------------------------------------*/

/**
	* @brief  Initializes the MIDI media low layer over the USB HS IP
	* @retval USBD_OK if all operations are OK else USBD_FAIL
	*/
static int8_t MIDI_Init_HS(void)
{
	/* Set Application Buffers */
	USBD_MIDI_SetTxBuffer(&hUsbDeviceFS, UserTxBufferHS, 0);
	USBD_MIDI_SetRxBuffer(&hUsbDeviceFS, UserRxBufferHS);
	return (USBD_OK);
}

/**
	* @brief  DeInitializes the MIDI media low layer
	* @param  None
	* @retval USBD_OK if all operations are OK else USBD_FAIL
	*/
static int8_t MIDI_DeInit_HS(void)
{
	return (USBD_OK);
}

/**
	* @brief  Manage the MIDI class requests
	* @param  cmd: Command code
	* @param  pbuf: Buffer containing command data (request parameters)
	* @param  length: Number of data to be sent (in bytes)
	* @retval Result of the operation: USBD_OK if all operations are OK else USBD_FAIL
	*/
static int8_t MIDI_Control_HS(uint8_t cmd, uint8_t* pbuf, uint16_t length)
{


	return (USBD_OK);
	/* USER CODE END 10 */
}

/**
	* @brief  Data to send over USB IN endpoint are sent over MIDI interface
	*         through this function.
	* @param  Buf: Buffer of data to be sent
	* @param  Len: Number of data to be sent (in bytes)
	* @retval Result of the operation: USBD_OK if all operations are OK else USBD_FAIL or USBD_BUSY
	*/
uint8_t MIDI_Transmit_HS(uint8_t* Buf, uint16_t Len)
{
	uint8_t result = USBD_OK;

	USBD_MIDI_HandleTypeDef *hMIDI = (USBD_MIDI_HandleTypeDef*)hUsbDeviceFS.pClassData;

	if (hMIDI->TxState != 0){

		return USBD_BUSY;

	}

	USBD_MIDI_SetTxBuffer(&hUsbDeviceFS, Buf, Len);
	result = USBD_MIDI_TransmitPacket(&hUsbDeviceFS);

	return result;
}

/**
	* @brief  Data received over USB OUT endpoint are sent over MIDI interface
	*         through this function.
	*
	*         @note
	*         This function will block any OUT packet reception on USB endpoint
	*         untill exiting this function. If you exit this function before transfer
	*         is complete on MIDI interface (ie. using DMA controller) it will result
	*         in receiving more data while previous ones are still not sent.
	*
	* @param  Buf: Buffer of data to be received
	* @param  Len: Number of data received (in bytes)
	* @retval USBD_OK if all operations are OK else USBD_FAIL
	*/
 uint8_t buffInputUSB[256];
         
static int8_t MIDI_Receive_HS(uint8_t* Buf, uint32_t *Len)
{
	static uint8_t read_message = 0;
	static uint8_t data_bytes   = 0;
        static MIDI_MESSAGE msgMIDI;
        static uint16_t bcCount = 0;

	for(uint8_t i = 0; i < *Len; ++i) 
        {
          buffInputUSB[bcCount++] = Buf[i];
          bcCount %= 256;

          if(Buf[i] & 0x80)
          {
            // начало пакета: устанавливаем флаг, что читаем пакет
            read_message = 1;
            data_bytes = 0;
            // разбираем байт статуса
            msgMIDI.channel = (Buf[i] & 0x0F);        // midi channel
            msgMIDI.cmd     = ((Buf[i] & 0x70) >> 4); // midi cmd
          }
          else
            if(read_message == 1)
            {
							
							
							
							
              // читаем данные из пакета
              msgMIDI.data[data_bytes] = Buf[i];
                
              ++data_bytes;
              if(data_bytes == 2)
              {
                //read_message = 0;
                data_bytes = 0;
                // Note on, velocity zero is equivalent to note off. 
                if((msgMIDI.cmd == MIDI_CIN_NOTE_ON) && (msgMIDI.data[1] == 0))
                    msgMIDI.cmd = MIDI_CIN_NOTE_OFF;
                // посылка принята - выставляем флаг готовности
                QAddMsg(msgMIDI);
              }
            }
        }
        
	USBD_MIDI_ReceivePacket(&hUsbDeviceFS);
	return (USBD_OK);
}

