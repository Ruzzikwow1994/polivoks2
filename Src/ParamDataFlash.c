/** Author: sdaware@mac.com
  ******************************************************************************
  * File Name          : ParamDataFlash.c
  * Date               : 22/06/2016 
  * Description        : ������� ������ � ���� �������.
  ******************************************************************************
  *     Flash STM32F205RBT
  *     Sector 0 0x0800 0000 - 0x0800 3FFF 16 Kbyte
  *     Sector 1 0x0800 4000 - 0x0800 7FFF 16 Kbyte
  * --> Sector 2 0x0800 8000 - 0x0800 BFFF 16 Kbyte   STORAGE
  * --> Sector 3 0x0800 C000 - 0x0800 FFFF 16 Kbyte   STORAGE  
  *     Sector 4 0x0801 0000 - 0x0801 FFFF 64 Kbyte
  ******************************************************************************
*/

/* Includes ------------------------------------------------------------------*/
#include "ParamDataFlash.h"        
#include <string.h>

/* Global variables ---------------------------------------------------------*/
/* 32 bit aligned memory area */

PARAMS_DEVICE params = {0};
PARAMS_DEVICE* pMemCurrParams;

#define MEMBLOCK_SIGN             0xABCDE016

#define SECTOR_MASK               ((uint32_t)0xFFFFFF07)

#define FLASH_TIMEOUT_VALUE_MY       ((uint32_t)50000U)/* 50 s */

#define FLASH_USER_START_ADDR		0x08008000

uint32_t Address = 0, PageError = 0;

/*Variable used for Erase procedure*/
static FLASH_EraseInitTypeDef EraseInitStruct;;

void InitParams(void)
{
  params.MemBlockSign   = MEMBLOCK_SIGN;
// ���������� ��������� ������ ��������
  params.Channel_curr = -1;
}

// �������� ���� �� ���� ���������� PARAMS_DEVICE � extMem
void ExistParams(void)
{
    uint16_t CountPD = 8192*4/sizeof(PARAMS_DEVICE);
    uint16_t i = 0;
	
    pMemCurrParams = (PARAMS_DEVICE* )FLASH_USER_START_ADDR;
    
    for(i = 0; i < CountPD; ++i)
    {
        if(pMemCurrParams->MemBlockSign == MEMBLOCK_SIGN)
        {
            // ������ ���� � ����������� ����������
            memcpy(&params, pMemCurrParams, sizeof(PARAMS_DEVICE));
            break;
        }
        
        ++pMemCurrParams;
    }
    
    if(params.MemBlockSign == 0x0)
    {
				EraseInitStruct.TypeErase = FLASH_TYPEERASE_PAGES;
				EraseInitStruct.PageAddress = FLASH_USER_START_ADDR;
				EraseInitStruct.NbPages = 1;
        // ���� ���������� ���������� �� ��� ������
        // Unlock the Flash to enable the flash control register access 
        HAL_FLASH_Unlock();
        
        // Erase the user Flash area (area defined by extMem)
        if(HAL_FLASHEx_Erase(&EraseInitStruct, &PageError) == HAL_OK)
        {
          if(HAL_FLASHEx_Erase(&EraseInitStruct, &PageError) == HAL_OK)
          {
              uint16_t* pAddressDist     = NULL;
              uint16_t* pAddressSource   = NULL;
              uint16_t* pPARAMS_END_ADDR = NULL; 
						
              InitParams();
              
              pAddressDist     = (uint16_t* )FLASH_USER_START_ADDR;
              pAddressSource   = (uint16_t* )&params;
              pPARAMS_END_ADDR = ((uint16_t* )FLASH_USER_START_ADDR) + sizeof(PARAMS_DEVICE); 

              // ����������� ����� ���������� �� ���������
              while (pAddressDist < pPARAMS_END_ADDR)
              {
                if (HAL_FLASH_Program(FLASH_TYPEPROGRAM_HALFWORD,(uint32_t)pAddressDist, *pAddressSource) == HAL_OK)
                {
                  pAddressDist        = pAddressDist + 1;
                  pAddressSource      = pAddressSource + 1;
                }
                else
                {
                  /* Error occurred while writing data in Flash memory.
                     User can add here some code to deal with this error */
                  while (1)
                  {
                  }
                }
              }
          }
        }
        // Unlock the Flash to enable the flash control register access 
        HAL_FLASH_Lock();        
    }
}

void NewParams2Flash(void)
{
  // ����� ��������� PARAMS_DEVICE
    uint16_t i = 0;
		uint8_t  j = 0;
		uint16_t* pAddressDist     = NULL;
		uint16_t* pAddressSource   = NULL;
		uint16_t* pPARAMS_END_ADDR = NULL; 
	
    uint16_t CountPD = 8192*4/sizeof(PARAMS_DEVICE);
    
    pMemCurrParams = (PARAMS_DEVICE* )FLASH_USER_START_ADDR;
    
    for(i = 0; i < CountPD; ++i)
    {
        if(pMemCurrParams->MemBlockSign == MEMBLOCK_SIGN)
        {
            // ������ ���� � ����������� ����������
          HAL_FLASH_Unlock();
            // ����������� �������
            //params.DeviceID =  pMemCurrParams->DeviceID + 1;
            // ����������� ��������� ���� ������
            pAddressDist = (uint16_t* )pMemCurrParams;
            
            for(j = 0; j < sizeof(PARAMS_DEVICE); ++j)
            {
              if (HAL_FLASH_Program(FLASH_TYPEPROGRAM_HALFWORD,(uint32_t)pAddressDist++, 0) != HAL_OK)
              {
                break;
              }
            }
            if((i + 1) == CountPD )
            {
              // ������� � ������� ����� ������,
              // ���������� �������� ���� � ������ ������� �����
              pMemCurrParams = (PARAMS_DEVICE* )FLASH_USER_START_ADDR;
              
              // Erase the user Flash area (area defined by extMem)
							if(HAL_FLASHEx_Erase(&EraseInitStruct, &PageError) == HAL_OK)
							{
								if(HAL_FLASHEx_Erase(&EraseInitStruct, &PageError) == HAL_OK)
								{
                }
                else
                {
                    /*
                      Error occurred while sector erase.
                      User can add here some code to deal with this error.
                      SectorError will contain the faulty sector and then to know the code error on this sector,
                      user can call function 'HAL_FLASH_GetError()'
                    */
                    while (1)
                    {
                    }
                }
              }
              else
              {
                    /*
                      Error occurred while sector erase.
                      User can add here some code to deal with this error.
                      SectorError will contain the faulty sector and then to know the code error on this sector,
                      user can call function 'HAL_FLASH_GetError()'
                    */
                    while (1)
                    {
                    }
              }
            }
            else
              ++pMemCurrParams;
            
            // ����������� ����� ���������� � ��������� �����

            pAddressDist     = (uint16_t* )pMemCurrParams;
            pAddressSource   = (uint16_t* )&params;
            pPARAMS_END_ADDR = ((uint16_t* )pMemCurrParams) + sizeof(PARAMS_DEVICE); 
            
            while (pAddressDist < pPARAMS_END_ADDR)
            {
              if (HAL_FLASH_Program(FLASH_TYPEPROGRAM_HALFWORD,(uint32_t)pAddressDist, *pAddressSource) == HAL_OK)
              {
                pAddressDist        = pAddressDist + 1;
                pAddressSource      = pAddressSource + 1;
              }
              else
              {
                /* Error occurred while writing data in Flash memory.
                   User can add here some code to deal with this error */
                while (1)
                {
                }
              }
            }
            
            HAL_FLASH_Lock();

            break;
        }
        
        ++pMemCurrParams;
    }
}

void ReadFlash2Params()
{
    uint16_t CountPD = 8192*4/sizeof(PARAMS_DEVICE);
		uint16_t i = 0;
    
    pMemCurrParams = (PARAMS_DEVICE* )FLASH_USER_START_ADDR;
    
    for(i = 0; i < CountPD; ++i)
    {
        if(pMemCurrParams->MemBlockSign == MEMBLOCK_SIGN)
        {
            // ������ ���� � ����������� ����������
            memcpy(&params, pMemCurrParams, sizeof(PARAMS_DEVICE));
            break;
        }
        
        ++pMemCurrParams;
    }
}


/******************************************************************************
*  ���������
*
*******************************************************************************/
// ���������� ����� ������ ���������� ��������� ����������
void Storage_InitParams(void)
{
  ExistParams();
}

// ������ ���������� �� ����, ���������� ����� ������ Storage_InitParams
void Storage_SetParams(void)
{
  NewParams2Flash();
}

