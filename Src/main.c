/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "adc.h"
#include "dac.h"
#include "dma.h"
#include "spi.h"
#include "usart.h"
#include "usb_device.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "qmessages.h"
#include "qnotes.h"
#include "ParamDataFlash.h"        
#include <math.h>
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
static int8_t Channel_curr = -1;  // любой канал
int8_t fMIDI_LEARN = 0;           // флаг получения номера канал
//uart
extern uint8_t d;
enum my_channel {
	CHANNEL_1 = 1,
	CHANNEL_2 = 2
} my_channel;

uint32_t dac1_last,dac2_last;
uint32_t vrefbuf[16];
uint32_t vref = 0;
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */
void MY_DAC_SetValue (enum my_channel Channel, uint16_t value);
/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */
	static MIDI_MESSAGE msgMIDI;
  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_ADC_Init();
  MX_DAC_Init();
  MX_SPI2_Init();
  MX_USART1_UART_Init();
  MX_USART2_UART_Init();
  MX_USB_DEVICE_Init();
  /* USER CODE BEGIN 2 */
	HAL_UART_Receive_DMA(&huart2,&d,1);
	  // читаем VREF 	
	HAL_ADC_Start_DMA(&hadc, vrefbuf, 16);
	
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
		
      static uint32_t dac;
      static float    fpitch = 0.0;
      static uint32_t notes_counter = 0;
          
      if(QGetMsg(&msgMIDI))
      {
        // получили сообщение - обрабатываем его
        if(fMIDI_LEARN)
        {
          Channel_curr = msgMIDI.channel;
          fMIDI_LEARN = 0;
          params.Channel_curr = Channel_curr;   
          Storage_SetParams();
        }
        else
        if( (Channel_curr == -1) || (msgMIDI.channel == Channel_curr))
        {
            switch (msgMIDI.cmd) 
            {
                case MIDI_CIN_NOTE_ON:
                     if((msgMIDI.data[0] >= 12) && (msgMIDI.data[0] <= 84))
                     {
                         ++notes_counter;
                         // нота нажата LED светится
                         HAL_GPIO_WritePin(LED_NOTE_ON_GPIO_Port, LED_NOTE_ON_Pin, GPIO_PIN_SET);
                         
                         // работаем в диапазоне 12 - 84
                         uint8_t data = msgMIDI.data[0];
                         dac = (((double)8.1761*exp((double)0.0578*data))*0.00568182)*(4095/10);
                                               
                         QNoteAdd(msgMIDI.data[0]);
                         
												 if(notes_counter>1)
												 {
													 //Первый канал
														uint32_t dac_pitch = dac;//(uint32_t)(dac * pow(1.0595, fpitch));
														if(dac_pitch > 4095) dac_pitch = 4095;                        
														MY_DAC_SetValue(CHANNEL_1,dac_pitch);					
													 //Второй канал
														uint8_t data = QNoteGetPreTop();
														dac = (((double)8.1761*exp((double)0.0578*data))*0.00568182)*(4095/10);
													  dac_pitch = dac;//(uint32_t)(dac * pow(1.0595, fpitch));
														if(dac_pitch > 4095) dac_pitch = 4095;       
														MY_DAC_SetValue(CHANNEL_2,dac_pitch);
													 HAL_GPIO_WritePin(GATE_GPIO_Port, GATE_Pin, GPIO_PIN_RESET);
													 
												 }
												 else
												 {
                         uint32_t dac_pitch = dac;//(uint32_t)(dac * pow(1.0595, fpitch));
                         if(dac_pitch > 4095) dac_pitch = 4095;                        
                         MY_DAC_SetValue(CHANNEL_1,dac_pitch);
                          HAL_GPIO_WritePin(GATE_GPIO_Port, GATE_Pin, GPIO_PIN_RESET);
												 }
                     }

                break;

                case MIDI_CIN_NOTE_OFF:
                     if( (notes_counter > 0) && (msgMIDI.data[0] >= 12) && (msgMIDI.data[0] <= 84) )
                     {
                         --notes_counter;
                         QNoteDel(msgMIDI.data[0]);
                         
                         if(notes_counter == 0) 
                         {
                            // выключение Gate 
                             HAL_GPIO_WritePin(GATE_GPIO_Port, GATE_Pin, GPIO_PIN_SET);    
                             // нота отжата LED выкл.
                             HAL_GPIO_WritePin(LED_NOTE_ON_GPIO_Port, LED_NOTE_ON_Pin, GPIO_PIN_RESET);
                         }
                         else if(notes_counter==1)
                         {
													 // выключение Gate 2
														
                             uint8_t data = QNoteGetTop();
                             // работаем в диапазоне 12 - 84
         										dac = (((double)8.1761*exp((double)0.0578*data))*0.00568182)*(4095/10);		 
														 uint32_t dac_pitch = dac;//(uint32_t)(dac * pow(1.0595, fpitch));
														 if(dac_pitch > 4095) dac_pitch = 4095;       
														 
														 MY_DAC_SetValue(CHANNEL_1,dac_pitch);
														 
														
															HAL_GPIO_WritePin(GATE_GPIO_Port, GATE_Pin, GPIO_PIN_RESET);
                             
                         }
												 else
												 {
													 //последняя нажатая нота
														uint8_t data = QNoteGetTop();
                             // работаем в диапазоне 12 - 84
                             dac = (((double)8.1761*exp((double)0.0578*data))*0.00568182)*(4095/10);                   
														 uint32_t dac_pitch = dac;//(uint32_t)(dac * pow(1.0595, fpitch));
														 if(dac_pitch > 4095) dac_pitch = 4095;       
														 MY_DAC_SetValue(CHANNEL_1,dac_pitch);
														 
													 
													//предпоследняя нажатая нота
															data = QNoteGetPreTop();
                             // работаем в диапазоне 12 - 84
                             dac = (((double)8.1761*exp((double)0.0578*data))*0.00568182)*(4095/10);          
														 dac_pitch = dac;//(uint32_t)(dac * pow(1.0595, fpitch));
														 if(dac_pitch > 4095) dac_pitch = 4095;       
														 MY_DAC_SetValue(CHANNEL_2,dac_pitch);
														  HAL_GPIO_WritePin(GATE_GPIO_Port, GATE_Pin, GPIO_PIN_RESET);
													 }
                     }
                break;

                case MIDI_CIN_PITCHBEND_CHANGE:   // сдвиг на одну октаву
                  {
                     int16_t p  = msgMIDI.data[1];
                     p = (p << 7) + msgMIDI.data[0];                     
                     fpitch = (p - 8192)/682.67;
                     uint32_t dac_pitch = (uint32_t)(dac * pow(1.0595, fpitch));
                     if(dac_pitch > 4095) dac_pitch = 4095;  
											MY_DAC_SetValue(CHANNEL_1,dac_pitch);										
                     
                  }
                break;

                case MIDI_CIN_CONTROL_CHANGE:
                  if(msgMIDI.data[0] == 1)
                  {
                     //HAL_DAC_SetValue(&hdac, DAC_CHANNEL_2, DAC_ALIGN_12B_R,  msgMIDI.data[1]*32); 
											MY_DAC_SetValue(CHANNEL_1,msgMIDI.data[1]*32);
                  }
                break;

                default:
                break;
            }              
        }
      }

    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI14|RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.HSI14State = RCC_HSI14_ON;
  RCC_OscInitStruct.HSI14CalibrationValue = 16;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL6;
  RCC_OscInitStruct.PLL.PREDIV = RCC_PREDIV_DIV1;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USB|RCC_PERIPHCLK_USART1
                              |RCC_PERIPHCLK_USART2;
  PeriphClkInit.Usart1ClockSelection = RCC_USART1CLKSOURCE_PCLK1;
  PeriphClkInit.Usart2ClockSelection = RCC_USART2CLKSOURCE_PCLK1;
  PeriphClkInit.UsbClockSelection = RCC_USBCLKSOURCE_PLL;

  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */
void MY_DAC_SetValue (enum my_channel Channel, uint16_t value)
{
	uint8_t spi_data;
	
	switch (Channel)
	{
		case CHANNEL_1:
			dac1_last = value;
			value = ~value;
			spi_data = (value&0x0F00)>>8;
			spi_data |= 0x30;
			CS_LOW;
			HAL_SPI_Transmit(&hspi2,&spi_data,1,0x1000);
			spi_data = value;
			HAL_SPI_Transmit(&hspi2,&spi_data,1,0x1000);
			CS_HIGH;
			break;
		case CHANNEL_2:
			dac2_last = value;
			value = ~value;
			spi_data = (value&0x0F00)>>8;
			spi_data |= 0xB0;
			CS_LOW;
			HAL_SPI_Transmit(&hspi2,&spi_data,1,0x1000);
			spi_data = value;
			HAL_SPI_Transmit(&hspi2,&spi_data,1,0x1000);
			CS_HIGH;
			break;
	}
}

void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* hadc)
{
	HAL_ADC_Stop_DMA(hadc);
	for(int i=0; i<16; i++)
	{
		vref += vrefbuf[i];
	}
	vref=vref>>4;
}
/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
