#include "qnotes.h"



static uint8_t notes[32];
static uint8_t phead = 0, ptail = 0;
static uint8_t queue_lenght = 32;

void  QNoteCreateQueue(uint8_t size_of_queue)
{
  if((size_of_queue > 31) || (size_of_queue == 0)) queue_lenght = 31;
  else                                             queue_lenght = size_of_queue; 
  
  phead = 0; ptail = 0;  
}


void QNoteDel(uint8_t note)
{
  uint8_t _ptail =  ptail;
  
  if(phead != ptail)
  {
    // в очереди есть сообщения
    do
    {
      if(notes[_ptail] == note)
      {
        // нашли нужную ноту: перемещаем ноты от _ptail + 1 до ptail
        int i = _ptail;
        do
        {
          if(i == 0)
            notes[i] = notes[(queue_lenght-1)];
          else
            notes[i] = notes[(i-1)];
          if(i == ptail) break;
          --i;
          if(i < 0) i = queue_lenght-1;
        } while(i != ptail);
        
        notes[ptail] = 0;
        break;
      }
      ++_ptail; _ptail %= queue_lenght;
    } while(phead != _ptail);
    // подтягиваем хвост на кол-во удаленных нот 
    ++ptail; ptail %= queue_lenght;
  }
}

void QNoteAdd(uint8_t note)
{
  notes[phead] = note;
  ++phead; phead %= queue_lenght;
  
  if(phead == ptail)
  {
    // очередь заполнилась, теряем самое старое сообщение
    ++ptail; ptail %= queue_lenght;    
  }
}

uint8_t QNoteGetTop()
{
  uint8_t note = 0;
  uint8_t pointer = 0;
  if(phead != ptail)
  {
    // очередь не пуста: возвращаем последнюю добавленную ноту
    if((phead - 1) < 0) pointer = (queue_lenght - 1);
    else                pointer = (phead - 1);  
    return notes[pointer];
  }
  
  return note;
}

uint8_t QNoteGetPreTop()
{
  uint8_t note = 0;
  uint8_t pointer = 0;
  if(phead != ptail)
  {
    // очередь не пуста: возвращаем предпоследнюю добавленную ноту
    if((phead - 2) < 0) pointer = (queue_lenght - 2);
    else                pointer = (phead - 2);  
    return notes[pointer];
  }
  
  return note;
}
