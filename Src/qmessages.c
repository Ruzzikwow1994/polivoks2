#include "qmessages.h"



static MIDI_MESSAGE msgs[32];
static uint8_t phead = 0, ptail = 0;
static uint8_t queue_lenght = 32;

void  QCreateQueueMsgs(uint8_t size_of_queue)
{
  if((size_of_queue > 31) || (size_of_queue == 0)) queue_lenght = 31;
  else                                             queue_lenght = size_of_queue; 
  
  phead = 0; ptail = 0;  
}


uint8_t QGetMsg(MIDI_MESSAGE* msgMIDI)
{
  if(phead != ptail)
  {
    // в очереди есть сообщения
    *msgMIDI = msgs[ptail];
    ++ptail; ptail %= queue_lenght;
    
    return 1;
  }
  else
  {
    // в очереди сообщений нет
    return 0;
  }
}

void QAddMsg(MIDI_MESSAGE msgMIDI)
{
  msgs[phead] = msgMIDI;
  ++phead; phead %= queue_lenght;
  
  if(phead == ptail)
  {
    // очередь заполнилась, теряем самое старое сообщение
    ++ptail; ptail %= queue_lenght;    
  }
}
